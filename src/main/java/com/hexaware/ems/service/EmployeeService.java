package com.hexaware.ems.service;

import java.util.Set;

import com.hexaware.ems.exception.EmployeeNotFoundException;
import com.hexaware.ems.model.Employee;

public interface EmployeeService {
	
	public Employee save(Employee employee);
	
	public Set<Employee> findAll();
	
	public Employee findById(long employeeId) throws EmployeeNotFoundException;
	
	//Employee updateManagerId(long employeeId, Employee updateManagerId);
	
	public void deleteById(long employeeId);

	Employee updateEmployee(long employeeId, Employee employee);
	
}
