package com.hexaware.ems.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hexaware.ems.exception.EmployeeNotFoundException;
import com.hexaware.ems.model.Employee;
import com.hexaware.ems.model.EmployeeLeaveDetails;
import com.hexaware.ems.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	
	@Override
	public Employee save(Employee employee) {
		System.out.println("inside save of service");
		System.out.println(employee);
		for(EmployeeLeaveDetails employeeLeaveDetails: employee.getLeaveDetailsList()) {
            employeeLeaveDetails.setEmployee(employee);
        }
//		for(Employee emp: employee.getSubordinates()) {
//			emp.setManager(employee);
//		}
		return this.employeeRepository.save(employee);
	}

	@Override
	public Set<Employee> findAll() {
		return new HashSet<Employee>(this.employeeRepository.findAll());
	}

	@Override
	public Employee findById(long employeeId) throws EmployeeNotFoundException {
		return this.employeeRepository.findById(employeeId)
				.orElseThrow(() -> new EmployeeNotFoundException("Employee with given ID not found"));
	}

	@Override
	public void deleteById(long employeeId) {
		this.employeeRepository.deleteById(employeeId);
	}

	@Override
	public Employee updateEmployee(long employeeId, Employee employee) {
		
		return this.employeeRepository.findById(employeeId).map(empl -> {
			empl.setEmail(employee.getEmail());
			empl.setDateOfJoining(employee.getDateOfJoining());
			empl.setName(employee.getName());
			return this.employeeRepository.save(empl);
		}).orElseGet(() -> {
			employee.setId(employeeId);
			return this.employeeRepository.save(employee);
		});
	}
}
