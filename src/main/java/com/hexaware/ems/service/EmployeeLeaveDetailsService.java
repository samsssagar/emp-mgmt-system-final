package com.hexaware.ems.service;

import java.util.List;

import com.hexaware.ems.exception.EmployeeLeaveDetailsNotFoundException;
import com.hexaware.ems.model.EmployeeLeaveDetails;

public interface EmployeeLeaveDetailsService {
	
	EmployeeLeaveDetails saveEmployeeLeaveDetails(EmployeeLeaveDetails employeeLeaveDetails);
	
	EmployeeLeaveDetails updateEmployeeLeaveDetails(long leaveId, EmployeeLeaveDetails employeeLeaveDetails);
	
	List<EmployeeLeaveDetails> fetchAllLeaveDetails();
	
	EmployeeLeaveDetails findEmployeeLeaveById(long leaveId) throws EmployeeLeaveDetailsNotFoundException;
	
	void deleteEmployeeLeaveDetailsById(long leaveId);
	
}
