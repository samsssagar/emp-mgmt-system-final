package com.hexaware.ems.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hexaware.ems.exception.EmployeeNotFoundException;
import com.hexaware.ems.model.Employee;
import com.hexaware.ems.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping
	public Set<Employee> fetchAllEmployee() {
		System.out.println("Inside Fetch of controller");
		return this.employeeService.findAll();
	}

	@PostMapping
	public Employee saveEmployee(@Valid @RequestBody Employee employee) {
		System.out.println("Inside Post method");
		return this.employeeService.save(employee);
	}

	@PutMapping("/{id}")
	public Employee updateOrder(@PathVariable long id, @Valid @RequestBody Employee employee) {
		return this.employeeService.updateEmployee(id, employee);
	}

	@DeleteMapping("/{id}")
	public void deleteOrderById(@PathVariable long id) {
		this.employeeService.deleteById(id);
	}

	@GetMapping("/{id}")
	public Employee getOrderByOrderId(@PathVariable long id) throws EmployeeNotFoundException {
		System.out.println("Inside the get Order by Order id " + id);
		return this.employeeService.findById(id);
	}
}
