package com.hexaware.ems.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(NOT_FOUND)
public class EmployeeLeaveDetailsNotFoundException extends Exception{
	
	public EmployeeLeaveDetailsNotFoundException(String message){
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
}
