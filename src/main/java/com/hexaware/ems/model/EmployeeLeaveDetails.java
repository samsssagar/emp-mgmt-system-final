package com.hexaware.ems.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "employee_leave")
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(exclude = "employee")
@Setter
@Getter
@EqualsAndHashCode(of = "leaveId")

public class EmployeeLeaveDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long leaveId;
	
	@Column(name = "start_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@JsonProperty("start_date")
	private LocalDate startDate;
	
	@Column(name = "end_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@JsonProperty("end_date")
	private LocalDate endDate;
	
	@Column(name = "leave_reason")
	private String leaveReason;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "leave_status")
	@JsonProperty("leave_status")
	private LeaveStatus leaveStatus = LeaveStatus.PENDING;
	
	@Column(name = "comments")
	private String comments;

	@ManyToOne
	@JoinColumn(name = "emp_id", nullable = false)
	@JsonIgnore
	private Employee employee;

	public EmployeeLeaveDetails() {

	}

	public EmployeeLeaveDetails(LocalDate startDate, LocalDate endDate, String leaveReason, LeaveStatus leaveStatus) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.leaveReason = leaveReason;
		this.leaveStatus = leaveStatus;
	}
}