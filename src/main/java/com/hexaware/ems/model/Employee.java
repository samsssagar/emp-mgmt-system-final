package com.hexaware.ems.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "employee")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "emp_id")
	private long id;

	@Column(name = "emp_name")
	@JsonProperty("emp_name")
	@NotEmpty(message = "name must not be empty")
	private String name;

	@Column(name = "date_of_joining")
	@JsonProperty("date_of_joining")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	private LocalDate dateOfJoining;

	@Column(name = "email")
	private String email;

	@JsonBackReference
	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "manager_id")
	@JsonProperty("manager_id")
	// @NotEmpty(message = "manager should not be empty")
	private Employee manager;

	@OneToMany(mappedBy = "manager")
	private List<Employee> subordinates;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY, 
			cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
	@JsonProperty("leave_details")
	private List<EmployeeLeaveDetails> leaveDetailsList; 

	public List<Employee> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(List<Employee> subordinates) {
		this.subordinates = subordinates;
	}

	public List<EmployeeLeaveDetails> getLeaveDetailsList() {
		return leaveDetailsList;
	}

	public void setLeaveDetailsList(List<EmployeeLeaveDetails> leaveDetailsList) {
		this.leaveDetailsList = leaveDetailsList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(LocalDate dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOfJoining == null) ? 0 : dateOfJoining.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (dateOfJoining == null) {
			if (other.dateOfJoining != null)
				return false;
		} else if (!dateOfJoining.equals(other.dateOfJoining))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", dateOfJoining=" + dateOfJoining + ", email=" + email + "]";
	}

	public Employee(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	
	

	public Employee() {
	}
	
//	public void addEmployeeLeaveDetails(EmployeeLeaveDetails empLeaveDetails){
//        this.leaveDetailsList.add(empLeaveDetails);
//        empLeaveDetails.setEmployee(this);
//    }
	
	public void addManager(Employee employee) {
		subordinates.add(employee);
		employee.setManager(this);
	}
	
}