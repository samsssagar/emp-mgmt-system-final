package com.hexaware.ems.model;

public enum LeaveStatus {
	APPROVED,
	PENDING,
	REJECTED;
}


